import java.util.Map;

public interface CountMap<V> {
    void add(V value);
    int getCount(V value);
    int remove(V value);
    int size();
    void addAll(CountMap source);
    Map toMap();
    void toMap(Map destination);
}
