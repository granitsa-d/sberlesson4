import java.util.*;

public class CountMapImp<V> implements CountMap<V> {
    private int size;
    private Node[] table;
    private int capacity = 3;

    public CountMapImp() {
        table = new Node[capacity];
    }

    public void add(V value) {
        int index = indexFor(hash(value.hashCode()), table.length);

        Node newNode = new Node(index, value, null);

        if (table[index] == null) {
            table[index] = newNode;
            size++;
        } else {
            Node node = table[index];
            while (node.next != null) {
                if (node.key == newNode.key) {
                    node.val++;
                    return;
                } else {
                    node = node.next;
                }
            }
            if (node.key == newNode.key) {
                node.val++;
                return;
            } else {
                newNode.next = table[index];
                table[index] = newNode;
                size++;
                return;
            }
        }
    }

    public int getCount(V value) {
        int index = indexFor(hash(value.hashCode()), table.length);
        if (table[index] != null) {
            Node node = table[index];
            while (node.next != null) {
                if (node.key == value) {
                    return node.val;
                }
                node = node.next;
            }
            return node.val;
        }
        return 0;
    }

    public int remove(V value) {
        int index = indexFor(hash(value.hashCode()), table.length);
        int c = getCount(value);
        if (table[index] != null) {
            if (table[index].key == value) {
                table[index] = table[index].next;
                size--;
                return c;
            }
            Node node = table[index];
            Node nextNode = node.next;
            while (node.next != null) {
                if (nextNode.key == value) {
                    node.next = node.next.next;
                    size--;
                    return c;
                } else {
                    node = nextNode;
                    nextNode = nextNode.next;
                }
            }
        }
        return 0;
    }

    public int size() {
        return size;
    }

    public void addAll(CountMap source) {
        Map<V,Integer> map = source.toMap();
        Set<V> set = map.keySet();

        for (V key: set) {
            int index = indexFor(hash(key.hashCode()), table.length);

            Node newNode = new Node(index, key, null);
            if (table[index] == null) {
                table[index] = newNode;
                size++;
            } else {
                Node node = table[index];
                while (node.next != null) {
                    if (node.key == newNode.key) {
                        node.val = node.val + map.get(key);
                    } else {
                        node = node.next;
                    }
                }
                if (node.key == newNode.key) {
                    node.val = node.val + map.get(key);
                } else {
                    newNode.next = table[index];
                    table[index] = newNode;
                    size++;
                }
            }
        }
    }


    public Map toMap() {
        Map<V, Integer> map = new HashMap<>();
        for (int i = 0; i < table.length; i++) {
            Node node = table[i];
            if (node != null) {
                while (node.next != null) {
                    map.put((V) node.key, node.val);
                    node = node.next;
                }
                map.put((V) node.key, node.val);
            }
        }
        return map;
    }

    public void toMap(Map destination) {
        Map<V, Integer> map = this.toMap();
        for (V key: map.keySet()) {
            destination.put(key, map.get(key));
        }
    }

    private static class Node<V> {
        final int index;
        final V key;
        int val;
        Node<V> next;

        public Node(int index, V key, Node<V> next) {
            this.index = index;
            this.key = key;
            this.val = 1;
            this.next = next;
        }

        public void setNext(Node<V> next) {
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return val == node.val;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val);
        }
    }

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static int indexFor(int h, int length) {
        return h & (length - 1);
    }
}
